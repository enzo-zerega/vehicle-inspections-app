import { unstable_createMuiStrictModeTheme as createMuiTheme } from "@material-ui/core";

// Override some default MUI styles

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#FBFCFD",
      light: "#81bcd7",
      dark: "#FBFCFD",
      contrastText: "#0278ae",
    },
    secondary: {
      main: "#a5ecd7",
      light: "#d2f6eb",
      dark: "#53766c",
      contrastText: "#000000",
    },
    attention: {
      main: "#e8ffc1",
    },
    background: {
      paper: "#FFFFFF",
      default: "#FBFCFD",
    },
  },
  shadows: ["none", "0px 5px 10px 0px #e8ecf0", "0px 5px 10px 0px #e8ecf0"],
  typography: {
    fontFamily: ["Nunito", "Arial"].join(","),
  },
  overrides: {
    MuiStepIcon: {
      root: {
        color: "#81bcd7",
        "& text": {
          fill: "#000",
        },
        "&$active circle": {
          color: "#0278ae",
        },
        "&$active text": {
          fill: "#FFFFFF",
        },
        "&$completed": {
          color: "#a5ecd7",
        },
      },
    },
  },
});

export default theme;
