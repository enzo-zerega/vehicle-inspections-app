import { makeStyles } from "@material-ui/core/styles";

// Custom styles. Accessed using useStyles().

const useStyles = makeStyles(
  (theme) => ({
    actions: {
      width: theme.spacing(20),
      height: theme.spacing(20),
      margin: theme.spacing(2),
      backgroundColor: "#FFF",
      "& > span": {
        display: "flex",
        flexDirection: "column",
        textAlign: "center",
        padding: theme.spacing(1),
        gap: theme.spacing(1),
      },
    },
    navigation: {
      width: "max-content",
      margin: theme.spacing(4),
      backgroundColor: "#FFF",
      borderRadius: "50px",
      "& > span": {
        display: "flex",
        textAlign: "center",
        padding: theme.spacing(1),
        gap: theme.spacing(1),
      },
    },
    appTitle: {
      fontSize: "1.5rem",
      margin: theme.spacing(2),
    },
    main: {
      display: "flex",
      flexDirection: "column",
      padding: theme.spacing(5),
      justifyContent: "center",
      alignItems: "center",
      maxWidth: "960px",
      margin: "auto",
    },
    section: {
      width: "100%",
    },
    registerTable: {
      width: "100%",
    },
    registerTableTitle: {
      fontWeight: "bold",
    },
    stepper: {
      width: "100%",
    },
    stepLabel: {
      "& span": {
        fontSize: "1rem",
      },
    },
    stepperButton: {
      marginTop: theme.spacing(4),
      marginBottom: theme.spacing(10),
      marginRight: theme.spacing(3),
      borderRadius: "50px",
    },
  }),
  { index: 1 }
);

export default useStyles;
