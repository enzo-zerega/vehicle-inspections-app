import dateFormat from "dateformat";

const formatDate = (date, format) => {
  if (!date) {
    return date;
  }
  return dateFormat(date, format);
};

export default formatDate;
