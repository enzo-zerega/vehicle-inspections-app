import { useEffect, useState } from "react";
import { Table } from "../components";
import { listVehicles } from "../queries";
import { useStyles } from "../styles";

const Register = ({ setTitle }) => {
  const classes = useStyles();
  const [vehicles, setVehicles] = useState([]);
  const [error, setError] = useState();

  useEffect(() => {
    setTitle("Vehicles register");
    listVehicles().then((res) => {
      if (res.status === 200) setVehicles(res.vehicles);
      else setError("Cannot access the server");
    }); // Fetch vehicles
  }, [setTitle]);
  return (
    <section className={classes.section}>
      <Table rows={vehicles} error={error} />
    </section>
  );
};

export default Register;
