import {
  Button,
  Step,
  Stepper,
  StepLabel,
  StepContent,
  Typography,
} from "@material-ui/core";
import { TableChart } from "@material-ui/icons";
import { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { updateVehicles, validateVehicles } from "../queries";
import { useStyles } from "../styles";

const UpdateRegister = ({ setTitle }) => {
  useEffect(() => {
    setTitle("Register update");
  }, [setTitle]);

  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0); // Sets the step current position. From 0-3
  const [validation, setValidation] = useState({
    items: [],
    newItems: 0,
    updatedItems: 0,
  });
  const [error, setError] = useState();
  const steps = ["Import a register file", "Review changes", "Confirm"]; // Steps labels

  const handleNext = () => {
    // Increment the step
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    // Decrease the step
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleInputFile = (file) => {
    let fileReader = new FileReader();
    fileReader.onloadend = (e) => {
      // After file was read
      const content = fileReader.result;
      setError(); // Reset error status on every request
      validateVehicles(content) // Post the file content and receive validation result
        .then((res) => {
          if (res.status === 200) setValidation(res.vehicles);
          else
            res.status === 400
              ? setError("Validation failed. There is an error with your file")
              : setError("Cannot connect to the server");
        });
      handleNext();
    };
    fileReader.readAsText(file); // Read file
  };

  return (
    <section className={classes.section}>
      <Stepper
        activeStep={activeStep}
        orientation="vertical"
        className={classes.stepper}
        elevation={2}
      >
        {steps.map((label, index) => (
          <Step key={index}>
            <StepLabel className={classes.stepLabel}>{label}</StepLabel>
            <StepContent>
              {index === 1 && !error && (
                <div className={classes.validation}>
                  <Typography>
                    Vehicles found: {validation.items.length}
                  </Typography>
                  <Typography>Updates: {validation.updatedItems}</Typography>
                  <Typography>New: {validation.newItems}</Typography>
                </div>
              )}
              {index === 1 && error && <Typography>{error}</Typography>}
              {index === 2 && error && <Typography>{error}</Typography>}

              <div className={classes.actionsContainer}>
                <div>
                  {index !== 0 ? (
                    <Fragment>
                      <Button
                        onClick={handleBack}
                        className={classes.stepperButton}
                        variant="contained"
                      >
                        Back
                      </Button>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={() => {
                          if (index === 2) {
                            setError();
                            updateVehicles().then((res) => {
                              if (res.status === 200) handleNext();
                              else setError("Cannot connect to the server");
                            });
                          } else handleNext();
                        }}
                        className={classes.stepperButton}
                        disabled={error && true}
                      >
                        {index === 1 ? "Next" : "Update register"}
                      </Button>
                    </Fragment>
                  ) : (
                    <Button
                      variant="contained"
                      color="primary"
                      component="label"
                      className={classes.stepperButton}
                    >
                      Choose file
                      <input
                        id="file"
                        type="file"
                        hidden
                        onChange={(e) => handleInputFile(e.target.files[0])}
                      />
                    </Button>
                  )}
                </div>
              </div>
            </StepContent>
          </Step>
        ))}
      </Stepper>

      {activeStep === 3 && (
        <Button
          variant="contained"
          component={Link}
          color="primary"
          className={classes.navigation}
          to="/register"
        >
          <TableChart />
          <span>View register</span>
        </Button>
      )}
    </section>
  );
};

export default UpdateRegister;
