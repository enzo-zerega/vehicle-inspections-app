import { useStyles } from "../styles";

const Main = ({ children }) => {
  const classes = useStyles();
  return <main className={classes.main}>{children}</main>;
};

export default Main;
