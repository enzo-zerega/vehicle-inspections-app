import {
  TableContainer,
  Table as TableMUI,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Paper,
  IconButton,
  Box,
  Typography,
  Collapse,
  Chip,
} from "@material-ui/core";
import { KeyboardArrowUp, KeyboardArrowDown } from "@material-ui/icons";
import { Fragment, useState } from "react";
import { formatDate } from "../common";
import { useStyles } from "../styles";

function Row({ row }) {
  const [open, setOpen] = useState(false);
  const classes = useStyles();

  return (
    <Fragment>
      <TableRow className={classes.root}>
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          {row.chassisNumber}
        </TableCell>
        <TableCell component="th" scope="row">
          {row.isNew && <Chip size="small" label="New" color="secondary" />}
          {row.isUpdated && (
            <Chip size="small" label="Updated" color="primary" />
          )}
        </TableCell>
        <TableCell align="right">
          {formatDate(row.nextInspectionDate, "mmm d, yyyy")}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div">
                Vehicle details
              </Typography>
              <TableMUI size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell rowSpan={10} />
                    <TableCell className={classes.registerTableTitle}>
                      Identity
                    </TableCell>
                    <TableCell align="right">{row.id}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={classes.registerTableTitle}>
                      Model year
                    </TableCell>
                    <TableCell align="right">{row.modelYear}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={classes.registerTableTitle}>
                      Approval type
                    </TableCell>
                    <TableCell align="right">{row.approvalType}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={classes.registerTableTitle}>
                      First registration
                    </TableCell>
                    <TableCell align="right">
                      {formatDate(row.firstRegistration, "mmm d, yyyy")}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={classes.registerTableTitle}>
                      Privately imported
                    </TableCell>
                    <TableCell align="right">{row.privatelyImported}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={classes.registerTableTitle}>
                      Deregistration date
                    </TableCell>
                    <TableCell align="right">
                      {row.deregistrationDate
                        ? formatDate(row.deregistrationDate, "mmm d, yyyy")
                        : "-"}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={classes.registerTableTitle}>
                      Color
                    </TableCell>
                    <TableCell align="right">{row.color}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={classes.registerTableTitle}>
                      Latest inspection date
                    </TableCell>
                    <TableCell align="right">
                      {formatDate(row.latestInspectionDate, "mmm d, yyyy")}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={classes.registerTableTitle}>
                      Latest registration
                    </TableCell>
                    <TableCell align="right">
                      {formatDate(row.latestRegistration, "mmm d, yyyy")}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={classes.registerTableTitle}>
                      Monthly registration
                    </TableCell>
                    <TableCell align="right">
                      {formatDate(row.monthlyRegistration, "mmm d")}
                    </TableCell>
                  </TableRow>
                </TableHead>
              </TableMUI>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </Fragment>
  );
}

const Table = ({ rows, error }) => {
  const classes = useStyles();
  return (
    <TableContainer component={Paper} className={classes.registerTable}>
      <TableMUI>
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell className={classes.registerTableTitle}>
              Chassis number
            </TableCell>
            <TableCell />
            <TableCell className={classes.registerTableTitle} align="right">
              Next inspection date
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <Row key={row.id} row={row} />
          ))}
          {!rows.length && !error && (
            <TableRow>
              <TableCell colSpan={2}>No vehicles in the register</TableCell>
            </TableRow>
          )}
          {error && (
            <TableRow>
              <TableCell colSpan={2}>{error}</TableCell>
            </TableRow>
          )}
        </TableBody>
      </TableMUI>
    </TableContainer>
  );
};

export default Table;
