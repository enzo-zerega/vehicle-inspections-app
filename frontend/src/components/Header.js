import { AppBar, Toolbar, Typography, IconButton } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import { Fragment } from "react";
import { useHistory } from "react-router-dom";

import { useStyles } from "../styles";

const Header = ({ title }) => {
  // Renders the header
  const classes = useStyles();
  const history = useHistory();
  const pathname = history.location.pathname;

  const handleGoBack = () => {
    history.goBack();
  };
  return (
    <AppBar position="static" elevation={1} className={classes.appBar}>
      <Toolbar>
        {pathname !== "/" ? ( // Opaque back arrow in home page
          <Fragment>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="return"
              onClick={handleGoBack}
            >
              <ArrowBack />
            </IconButton>
            <Typography variant="h1" className={classes.appTitle} noWrap>
              {title}
            </Typography>
          </Fragment>
        ) : (
          <Fragment>
            <IconButton edge="start" style={{ opacity: 0 }} disabled>
              <ArrowBack />
            </IconButton>
            <Typography variant="h1" className={classes.appTitle} noWrap>
              Vehicle inspections
            </Typography>
          </Fragment>
        )}
      </Toolbar>
    </AppBar>
  );
};

export default Header;
