import { Button } from "@material-ui/core";
import { Sync, TableChart } from "@material-ui/icons";
import { useStyles } from "../styles";
import { Link } from "react-router-dom";

const Actions = () => {
  // Renders actions buttons
  const classes = useStyles();
  return (
    <div>
      <Button
        variant="contained"
        component={Link}
        color="primary"
        className={classes.actions}
        to="/register/update"
      >
        <Sync />
        <span>Update register</span>
      </Button>
      <Button
        variant="contained"
        component={Link}
        color="primary"
        className={classes.actions}
        to="/register"
      >
        <TableChart />
        <span>View register</span>
      </Button>
    </div>
  );
};

export default Actions;
