export default async function updateVehicles() {
  try {
    const response = await fetch("/api/vehicles", {
      method: "POST",
      headers: {
        "Content-Type": "text/plain",
      },
    });
    const vehicles = await response.json();
    return { vehicles, status: response.status };
  } catch (err) {
    return { vehicles: [], status: err };
  }
}
