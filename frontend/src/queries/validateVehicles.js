export default async function validateVehicles(body) {
  try {
    const response = await fetch("/api/vehicles/validate", {
      method: "POST",
      headers: {
        "Content-Type": "text/plain",
      },
      body,
    });
    if (response.status === 400)
      // Validation error
      return {
        vehicles: {
          items: [],
          newItems: 0,
          updatedItems: 0,
        },
        status: response.status,
      };
    const vehicles = await response.json();
    return { vehicles, status: response.status };
  } catch (err) {
    return {
      vehicles: {
        items: [],
        newItems: 0,
        updatedItems: 0,
      },
      status: err,
    };
  }
}
