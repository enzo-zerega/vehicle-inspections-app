export default async function listVehicles() {
  try {
    const response = await fetch("/api/vehicles", {
      method: "GET",
    });
    const vehicles = await response.json();
    return { vehicles, status: response.status };
  } catch (err) {
    return { vehicles: [], status: err };
  }
}
