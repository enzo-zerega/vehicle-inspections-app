import { Route, Switch, useLocation } from "react-router-dom";
import { ThemeProvider } from "@material-ui/core/styles";
import { CssBaseline } from "@material-ui/core";
import { theme } from "./styles";
import { Home, Register, UpdateRegister } from "./views";
import { Header, Main } from "./components";
import { useState } from "react";

function App() {
  const location = useLocation();
  const [title, setTitle] = useState("Vehicle inspections");

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Header title={title} />
      <Main>
        <Switch location={location} key={location.key}>
          <Route path="/" exact component={() => <Home />} />
          <Route
            path="/register"
            exact
            component={() => <Register setTitle={setTitle} />}
          />
          <Route
            path="/register/update"
            exact
            component={() => <UpdateRegister setTitle={setTitle} />}
          />
        </Switch>
      </Main>
    </ThemeProvider>
  );
}

export default App;
