const express = require("express");
const router = express.Router();
const {
  validateVehicle,
  transformToDate,
  sortVehicleByDate,
  updatedVehicle,
  deleteVehicle,
  updateVehicle,
} = require("../models/vehicle");

let vehicles = []; // Vehicles are stored in runtime memory
let tempVehicles = []; // Validated vehicles wating for confirmation to be added to the vehicles array

router.get("/", async (req, res) => {
  sortVehicleByDate(vehicles);
  res.send(vehicles);
});

router.post("/", async (req, res) => {
  if (!tempVehicles.length) res.status(403).send("Validation required");
  else {
    tempVehicles.map((currVehicle) => {
      const prevVehicle = vehicles.find((item) => item.id === currVehicle.id);
      if (!currVehicle.isNew) {
        if (currVehicle.isUpdated) {
          // delete prevVehicle if it was updated and replace it with currVechicle
          deleteVehicle(currVehicle.id, vehicles);
          vehicles.push(currVehicle);
        } else if (prevVehicle.isNew) {
          // Assign false to prevVehicle isNew property if necessary
          updateVehicle(currVehicle.id, "isNew", false, vehicles);
          // Assign false to prevVehicle isUpdated property if necessary
        } else if (prevVehicle.isUpdated) {
          updateVehicle(currVehicle.id, "isUpdated", false, vehicles);
        }
      } else {
        vehicles.push(currVehicle);
      }
    });
    tempVehicles = [];
    res.send(vehicles);
  }
});

router.post("/validate", async (req, res) => {
  tempVehicles = [];
  const rows = req.body.split(/\r?\n/g); // returns array of vehicles
  let updatedItems = 0;
  let newItems = 0;
  let error;
  rows.map((row) => {
    if (!validateVehicle(row)) error = true;
    else {
      const prevVehicle = vehicles.find(
        (vehicle) => vehicle.id === row.slice(0, 7)
      );
      const isNew = prevVehicle ? false : true;
      let currVehicle = {
        id: row.slice(0, 7),
        chassisNumber: row.slice(7, 26).trim(),
        modelYear: row.slice(26, 30),
        approvalType: row.slice(30, 41),
        firstRegistration: transformToDate(row.slice(41, 49)),
        privatelyImported: row.slice(49, 50),
        deregistrationDate: transformToDate(row.slice(50, 58)),
        color: row.slice(58, 78).trim(),
        latestInspectionDate: transformToDate(row.slice(78, 86)),
        nextInspectionDate: transformToDate(row.slice(86, 94)),
        latestRegistration: transformToDate(row.slice(94, 102)),
        monthlyRegistration: transformToDate(row.slice(102, 106)),
      };
      let isUpdated = false;
      if (!isNew) {
        isUpdated = updatedVehicle(currVehicle, prevVehicle);
        currVehicle.isNew = isNew;
        currVehicle.isUpdated = isUpdated;
        tempVehicles.push(currVehicle);
        if (isUpdated) updatedItems++;
      } else {
        currVehicle.isNew = isNew;
        newItems++;
        currVehicle.isUpdated = isUpdated;
        tempVehicles.push(currVehicle);
      }
    }
  });
  if (error) res.status(400).send("Bad request");
  else {
    res.send({ items: tempVehicles, newItems, updatedItems });
  }
});

module.exports = router;
