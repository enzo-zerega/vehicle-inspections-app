const express = require("express");
const vehicles = require("./api/vehicles");
const home = require("./routes/home");

const app = express();
app.use(express.text()); // To parse req.body of "text/plain" type
app.use("/", home);
app.use("/api/vehicles", vehicles);

const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Listening on port ${port} ...`));
