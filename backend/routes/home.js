const express = require("express");
const router = express.Router();

router.use(express.static("../frontend/build"));

router.get("/", (req, res) => {
  // Serves the compiled react application
  res.sendFile(path.resolve(__dirname, "../frontend/build", "index.html"));
});

module.exports = router;
