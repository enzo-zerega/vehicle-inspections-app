const validateVehicle = (vehicle) => {
  const validationPattern = /[^\w]+/;
  if (!validationPattern.test(vehicle) || vehicle.length !== 106) {
    // Only ASCII characters allowed and must have 106 characters
    return false;
  }
  return true;
};

const sortVehicleByDate = (vehicle) => {
  vehicle.sort(function (a, b) {
    // Sorts the vehicles array by the "nextInspectionDate" property of each of its elements
    let Date1;
    let Date2;
    Date1 = a["nextInspectionDate"];
    Date2 = b["nextInspectionDate"];
    if (Date1 < Date2) {
      return -1;
    }
    if (Date1 > Date2) {
      return 1;
    }

    return 0;
  });
  return vehicle;
};

const updatedVehicle = (currVehicle, prevVehicle) => {
  // Checks if a vehicle has been updated. Excludes properties "isNew" and "isUpdated"
  let isUpdated = false;
  Object.keys(currVehicle).map((key, index) => {
    if (currVehicle[key].valueOf() !== prevVehicle[key].valueOf()) {
      if (key !== "isNew" || key !== "isUpdated") isUpdated = true;
    }
  });
  return isUpdated;
};

const updateVehicle = (id, property, newValue, vehicles) => {
  // Updates the value of a given property of a given vehicle
  const index = vehicles.map((item) => item.id).indexOf(id);
  vehicles[index][property] = newValue;
  return vehicles[index];
};

const deleteVehicle = (id, prevVehicles) => {
  // Deletes a given vehicle from the vehicles array
  const index = prevVehicles.map((item) => item.id).indexOf(id);
  const currVehicles = prevVehicles.splice(index, 1);
  return currVehicles;
};

const transformToDate = (dateString) => {
  // Possible cases: 00000000, 8 digits, 4 digits
  if (dateString === "00000000") {
    return "";
  } else if (dateString.length === 8) {
    const year = dateString.slice(0, 4);
    const month = dateString.slice(4, 6);
    const day = dateString.slice(6, 8);
    return new Date(`${year}-${month}-${day}`);
  } else if (dateString.length === 4) {
    const year = new Date().getFullYear(); // Add missing year to 4 digits date. Ony to handle it as a date. It is not displayed in the frontend app
    const month = dateString.slice(0, 2);
    const day = dateString.slice(2, 4);
    return new Date(`${year}-${month}-${day}`);
  } else {
    return dateString;
  }
};

module.exports = {
  validateVehicle,
  transformToDate,
  sortVehicleByDate,
  updatedVehicle,
  deleteVehicle,
  updateVehicle,
};
