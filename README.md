# Vehicle inspections app

### Installation steps

1. Clone the repository 

   `git clone https://enzo-zerega@bitbucket.org/enzo-zerega/vehicle-inspections-app.git`

2. Go to the backend folder 

   `cd vehicle-inspections-app/backend`

3. Install dependencies 

   `npm install`

4. Run the application server 

   `node index.js`  or `nodemon index.js`

5. Open `http://localhost:5000/` to view the app in a web browser

### Details

The app was entirely build using JavaScript. The backend uses Node.js v15.2.1 and Express v4.17.1, and the frontend uses React v17.0.2. 

**Additional dependencies**:

- *react-router-dom* for handling routes
- *typeface* for installing a custom font
- *dateformat* for formatting dates
- *@material-ui/core* for displaying React components based in Material Design styling guidelines (I used this to save time in the styling process)

### App flow

![App flow](./frontend/public/app-flow.png)

The app works using a node.js server that handles API requests to create, list, and update vehicles in a registry. The vehicle data would ideally be stored in a database, but for the purpose of this assignment all data is stored in variables only available during runtime. When the user imports a file to the application, the data is extracted, validated and compared with previously imported data to check if new items will be added or old ones will be updated. After a confirmation from the user, the imported data is saved. 

Node.js serves the build version of the React application to `http://localhost:5000/`. However, it is also possible to start a development server from the React application by following these instructions from the root folder:

`cd frontend`

`npm install`

`npm start`



